

### Instructions 
   **Pour des instructions détaillées sur l'installation des programmes WampServer et Visual Studio Code, veuillez consulter le fichier PDF fourni.** 
   
1.Téléchargez et installez WampServer-3-3-1-64bit sur votre ordinateur. Assurez-vous également d'installer PHP version 8.2 et MySQL version 8.0.33.

2.Importez le schéma de la base de données : Dans le fichier SQL fourni avec l'application (web_ex.sql), vous trouverez le schéma de la base de données. Importez ce fichier dans votre serveur MySQL.

3.Configurez les variables : Accédez au dossier de l'application clonée et localisez le fichier "config-dist.php". Remplacez les variables existantes par les informations appropriées, puis renommez le fichier en "config.php".

4.Démarrez WampServer : Lancez WampServer sur votre ordinateur. Assurez-vous que les serveurs Apache et MySQL sont en cours d'exécution.

5.Accédez à l'application : Ouvrez votre navigateur web et entrez l'URL du serveur web, généralement "localhost". Vous serez dirigé vers l'application "Projet-ExamanPHP".

6.Lorsque le premier utilisateur s'inscrit, il devient automatiquement un administrateur,et peut accéder aux profils des autres utilisateurs ainsi que les supprimer.

7.Assurez-vous de remplacer ces informations si vous avez utilisé d'autres valeurs lors de la configuration de votre base de données.
