<?php

// Récupérer les données du formulaire
if (!empty($_POST['username']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

    if (userExistsByUsername($_POST['username']) || userExistsByEmail($_POST['email'])) {
        $_SESSION['alert'] = 'L\'utilisateur existe déjà!';
        header('Location: index.php?pass=view/login');
        die;
    }
    // version basique
    $username = $_POST['username'];
    $pwd = $_POST['password'];
    $email = $_POST['email'];
    $admin = 0;

    //STATUE ADMIN
    // 1. connexion
    global $connect;
    $connect = connect();

    $usersQuery = $connect->prepare("SELECT * FROM user");
    $usersQuery->execute();
    $usersCount = $usersQuery->rowcount();
    if ($usersCount > 0) {
        $admin = 0;
    } else {
        $admin = 1;
    }
    $params = [
        trim($username),
        password_hash($pwd, PASSWORD_DEFAULT),
        $email,
        $admin,

    ];

    // 2. QUERY
    $insert = $connect->prepare("INSERT INTO user (username, password, email, admin, created, lastlogin) VALUES (?, ?, ?, ?, NOW(), NOW())");

    // ...

    // 3. EXECUTE
    $insert->execute($params);

    if ($insert->rowcount()) {
        $adminText = 'pas admin';
        if ($admin) {
            $adminText = 'admin';
        }
        echo 'Utilisateur ' . $username . ' a été créé avec succès et il est ' . $adminText;
    } else {
        echo 'Création utilisateur a échoué  et il est ' . $adminText;
    }

    //( 4. FETCH)

} else {
    echo 'La création a échoué';
}
