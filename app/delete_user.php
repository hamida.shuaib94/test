<?php
if ($_SESSION['admin'] !== 'Oui') {
    header('Location: index.php');
    die;
} else {

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $userId = $_POST['userid'];
        if ($_SESSION['userid'] == $userId) {
            echo "Vous ne pouvez pas supprimer l'utilisateur administrateur.";
            exit;
        }
        // Connexion à la base de données
        $connect = connect();

        // Exécuter la requête de suppression de l'utilisateur
        $delete = $connect->prepare("DELETE FROM user WHERE id = ?");
        /*var_dump($delete);*/
        $delete->execute([$userId]);

        if ($delete->rowCount()) {
            echo "L'utilisateur a été supprimé avec succès.";
        } else {
            echo "La suppression de l'utilisateur a échoué.";
        }
    } else {
        echo "Méthode non autorisée.";
    }
}
