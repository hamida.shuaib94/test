 <?php

    session_name('WEB' . date('Ymd'));
    session_start(['cookie_lifetime' => 3600]);


    if (!empty($_SESSION['userid'])) {
        require_once '../lib/user.php';
        require_once '../lib/pdo.php';
        require_once '../lib/output.php';
        require_once '../config.php';

        exportJSON();
    }
