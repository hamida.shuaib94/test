<?php
/*traitement d'authentification utilisateur*/

if (!empty($_POST['username']) && !empty($_POST['password'])) {
    $username = trim($_POST['username']);
    $password = $_POST['password'];



    $user = getUserByUsername($username);


    if (!is_object($user)) {
        $_SESSION['alert'] = 'Échec de l\'authentification';
        $_SESSION['alert-color'] = 'danger';
        header('Location: index.php?pass=view/login');
        die;
    }


    if (password_verify($_POST['password'], $user->password)) {
        if (!empty($user->id)) {
            $_SESSION['userid'] = $user->id;
            $_SESSION['username'] = $user->username;
            $_SESSION['email'] = $user->email;
            $_SESSION['created'] = $user->created;
            $_SESSION['lastlogin'] = $user->lastlogin;
            $adminMessage = 'Non';
            if ($user->admin == 1) {
                $adminMessage = 'Oui';
            }
            $_SESSION['admin'] = $adminMessage;


            $sql = "UPDATE user SET lastlogin = NOW() WHERE id = ?";
            //connexion
            $connect = connect();
            // QUERY
            $update = $connect->prepare($sql);
            // EXECUTE
            $update->execute([$user->id]);

            if ($update->rowCount()) {
                echo 'Bienvenue : ' . $username;
            }
        }

        $url = 'index.php?pass=view/profile';
        $_SESSION['alert'] =  'Bienvenue : ' . $username;
    } else {
        $_SESSION['alert'] = 'Échec de l\'authentification';
        $_SESSION['alert-color'] = 'danger';
        $url = 'index.php?pass=view/login';
    }
    header('Location: ' . $url);
    exit();
}
