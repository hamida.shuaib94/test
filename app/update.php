<?php

if (!empty($_SESSION['userid'])) {
    $userId = $_SESSION['userid'];

    $shouldUpdateEmail = isset($_POST['email']) && filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL);
    $shouldUpdatePassword = isset($_POST['password']) && !empty($_POST['password']);

    if ($shouldUpdateEmail ||  $shouldUpdatePassword) {
        $user = getUserByUsername($_SESSION['username']);
        if (!empty($user)) {
            // only change the email to the new value if there was a new email given
            $email = $_SESSION['email'];
            if ($shouldUpdateEmail) {
                $email = trim($_POST['email']);
            }
            // only change the password if a new value was given for the password
            $password = $user->password;
            if ($shouldUpdatePassword) {
                $password = $_POST['password'];
            }
            // only hash the password if a new password is given because the password saved in the session is already hashed
            $hashedPassword =  $password;
            if ($shouldUpdatePassword) {
                $hashedPassword  = password_hash($password, PASSWORD_DEFAULT);
            }
            // 1. CONNECT
            $connect = connect();

            // 2. QUERY
            $request = $connect->prepare("UPDATE user SET password = ?, email = ? WHERE id = ?");

            $params = [
                $hashedPassword,
                $email,
                $userId
            ];

            // 3. EXECUTE
            $request->execute($params);
            // update the email in the session so that it is displayed correctly in the profile page
            $_SESSION['email'] = $email;

            // redirect to profile without update GET parameter
            header('Location: index.php?pass=view/profile');
            die;
        }
    }
} else {
    header('Location: index.php?pass=view/login');
    die();
}
