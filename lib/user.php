
<?php



/**
 * @param string $argumentType should be one of [username, email, id] accourding to the passed argument
 * @param string $argument can be the username, userId or email of a user
 * @return object|$0|false|stdClass|null
 */
function getUserByArgument(string $argumentType, string $argument): mixed
{
    $searchBy = 'username';
    if ($argumentType === 'email') {
        $searchBy = 'email';
    }
    if ($argumentType === 'id') {
        $searchBy = 'id';
    }
    $connect = connect();

    // 2. QUERY
    $request = $connect->prepare("SELECT * FROM user WHERE " . $searchBy . " = ?");

    $params = [
        trim($argument),
    ];

    // 3. EXECUTE
    $request->execute($params);

    // 4. FETCH
    return $request->fetchObject();
}



/**
 * @param string $username
 * @return object|false|stdClass|null
 */
function getUserByUsername(string $username): mixed
{
    return getUserByArgument('username', $username);
}
/**
 * @param string $email
 * @return object|false|stdClass|null
 */
function getUserByEmail(string $email): mixed
{
    return getUserByArgument('email', $email);
}





/**
 * @param string $username
 * @return bool
 */
function userExistsByUsername(string $username): bool
{
    if (is_object(getUserByUsername($username))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Summary of userExistsByEmail
 * @param string $email
 * @return bool
 */
function userExistsByEmail(string $email): bool
{
    if (is_object(getUserByEmail($email))) {
        return true;
    } else {
        return false;
    }
}






/**
 * Summary of exportJSON
 * @return void
 */
function exportJSON(): void
{
    if (isset($_SESSION['userid'])) {
        $user = [
            'id' => $_SESSION['userid'],
            'username' => $_SESSION['username'],
            'email' => $_SESSION['email'],
            'created' => $_SESSION['created'],
            'lastlogin' => $_SESSION['lastlogin'],
            'admin' => $_SESSION['admin'],
        ];

        $filename = $_SESSION['userid'] . '_' . time() . '.json';
        // Envoi des headers HTTP au browser pour le téléchargement du fichier.
        header('Content-type: application/json');
        header('Content-disposition: attachment; filename="' . $filename . '"');
        // output du contenu au format json
        echo json_encode($user);
    }
}
