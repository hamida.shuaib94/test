<?php
if ($_SESSION['admin'] != 'Oui') {
    header('Location: index.php');
    die;
}
//connexion
$connect = connect();

// // 2. QUERY
$request = $connect->prepare("SELECT id,username,email,created,lastlogin,admin From user 

 ");

// 3. EXECUTE
$request->execute();

// 4. FETCH
$users = $request->fetchAll(PDO::FETCH_ASSOC);

?>
<table class="table table-bordered w-75 m-auto mt-5">
    <thead>
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>Created</th>
            <th>Last Login</th>
            <th>Admin</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <?php foreach ($user as $key => $value) : ?>
                    <td><?php
                        $formatedvalue = $value;
                        if ($key == 'lastlogin' || $key == 'created') {
                            $formatedvalue = date_format(new DateTime($value), "d/m/Y H\hi");
                        }
                        if ($key == 'admin') {
                            if ($value == 0) {
                                $formatedvalue = 'non';
                            } else {
                                $formatedvalue = 'oui';
                            }
                        }
                        echo $formatedvalue;
                        ?></td>
                <?php endforeach; ?>
                <td>
                    <form method="POST" action="index.php?pass=app/delete_user">
                        <input type="hidden" name="userid" value="<?php echo $user['id']; ?>">
                        <button type="submit">Supprimer</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>