<?php

//connexion
$connect = connect();

// // 2. QUERY
$request = $connect->prepare("SELECT name , code

 FROM course");

// 3. EXECUTE
$request->execute();

// 4. FETCH
$courses = $request->fetchAll(PDO::FETCH_ASSOC);

?>
<table class="table table-bordered w-50 m-auto mt-5">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Code</th>
        </tr>
    <tbody>
        <?php
        foreach ($courses as $course) {
            echo '<tr>';
            foreach ($course as $key => $value) {
                echo '<td>' . $value . '</td>';
            }
            echo '</tr>';
        }
        ?>
    </tbody>
    </thead>
</table>